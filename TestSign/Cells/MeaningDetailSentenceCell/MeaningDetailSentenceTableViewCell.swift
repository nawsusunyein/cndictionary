//
//  MeaningDetailSentenceTableViewCell.swift
//  TestSign
//
//  Created by techfun on 2018/11/29.
//  Copyright © 2018 Naw Su Su Nyein. All rights reserved.
//

import UIKit

class MeaningDetailSentenceTableViewCell: UITableViewCell {

    @IBOutlet weak var cnSentence : UILabel!
    @IBOutlet weak var copySentenceBtn : UIButton!
    @IBOutlet weak var pronounceSenBtn : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
