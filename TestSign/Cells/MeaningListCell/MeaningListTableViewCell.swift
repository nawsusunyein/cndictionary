//
//  MeaningListTableViewCell.swift
//  TestSign
//
//  Created by techfun on 2018/11/28.
//  Copyright © 2018 Naw Su Su Nyein. All rights reserved.
//

import UIKit

class MeaningListTableViewCell: UITableViewCell {

    @IBOutlet weak var cnChar: UILabel!
    @IBOutlet weak var pinyin: UILabel!
    
    @IBOutlet weak var meaningCard: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
