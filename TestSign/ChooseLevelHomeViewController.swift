//
//  ChooseLevelHomeViewController.swift
//  TestSign
//
//  Created by techfun on 2018/11/27.
//  Copyright © 2018 Naw Su Su Nyein. All rights reserved.
//

import UIKit

class ChooseLevelHomeViewController: UIViewController {

    @IBOutlet weak var levelChooseBackGround: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    //Private function
    func setGradientforBackGround(){
        let gradient = CAGradientLayer()
        
        gradient.frame = view.bounds
        gradient.colors = [UIColor.white.cgColor, UIColor.black.cgColor]
        
        levelChooseBackGround.layer.insertSublayer(gradient, at: 0)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
