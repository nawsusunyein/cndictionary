//
//  ViewController.swift
//  TestSign
//
//  Created by techfun on 2018/11/27.
//  Copyright © 2018 Naw Su Su Nyein. All rights reserved.
//

import UIKit
import Google
import GoogleSignIn
import FirebaseDatabase
import Firebase

class ViewController: UIViewController,GIDSignInUIDelegate, GIDSignInDelegate{

    @IBOutlet weak var lblUserEmail: UILabel!
    @IBOutlet weak var btnSignIn: UIButton!
    var databaseHandle : FIRDatabaseHandle!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FIRApp.configure()
        let db = FIRDatabase.database().reference()
        db.setValue("Hello Database")
        
        // Save Data Into Firebase
        db.child("mingzi").childByAutoId().setValue("bao lin")
        db.child("mingzi").childByAutoId().setValue("shuo ling")
        db.child("mingzi").childByAutoId().setValue("bao er")
        db.child("mingzi").childByAutoId().setValue("lao shu")
        
        //Read Data From Firebase
        databaseHandle = db.child("mingzi").observe(.childAdded, with: {(data) in
            let name : String = (data.value) as! String
            debugPrint(name)
            })
        
        //Update Data From Firebase
       db.child("mingzi").child("-LSNRSMge1DjI8443cr8").setValue("lin bao")
        
        //Delete Data From Firebase
        db.child("mingzi").child("bao lin").removeValue()
        //error object
        var error : NSError?
        
        //setting the error
        GGLContext.sharedInstance().configureWithError(&error)
        
        //if any error stop execution and print error
        if error != nil{
            print(error ?? "google error")
            return
        }
        
        
        //adding the delegates
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
        //getting the signin button and adding it to view
        let googleSignInButton = GIDSignInButton()
        googleSignInButton.center = view.center
        view.addSubview(googleSignInButton)
        self.btnSignIn.applyGradient(colors: [UIColor.green.cgColor, UIColor.yellow.cgColor])
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    //when the signin complets
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        let userinfo = user
        //if any error stop and print the error
        if error != nil{
            print(error ?? "google error")
            return
        }
        
        //if success display the email on label
        lblUserEmail.text = user.profile.email
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        GIDSignIn.sharedInstance().signOut()
    }
    
    private func setButtonGradient(){
        let gradientx = CAGradientLayer()
        gradientx.colors = [UIColor.blue,UIColor.red]
       // gradientx.startPoint = CGPoint(x: 0.0, y: 0.5)
       // gradientx.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientx.frame = btnSignIn.bounds
        btnSignIn.layer.insertSublayer(gradientx, at: 0)
    }
    
    @IBAction func signInAction(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.statusBarBackgroundColor = .red
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
}

extension UIButton
{
    func applyGradient(colors: [CGColor])
    {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0)
        gradientLayer.frame = self.bounds
        self.layer.addSublayer(gradientLayer)
    }
}

extension UIApplication {
    class var statusBarBackgroundColor: UIColor? {
        get {
            return (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor
        } set {
            (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor = newValue
        }
    }
}
