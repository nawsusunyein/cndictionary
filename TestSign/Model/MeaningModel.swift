//
//  MeaningModel.swift
//  TestSign
//
//  Created by techfun on 2018/11/28.
//  Copyright © 2018 Naw Su Su Nyein. All rights reserved.
//

import Foundation
import FirebaseDatabase
import Firebase

class MeaningModel{
    public var pinyin : String?
    public var cnChar : String?
    public var meaning : String?
    public var frequency : Int?
    public var level : Int?
    public var sentences : [String:AnyObject]?
    
    init(_ pinyin : String,_ cnChar : String,_ meaning:String,_ sentences:[String:AnyObject],_ frequency : Int,_ level:Int){
        self.pinyin = pinyin
        self.cnChar = cnChar
        self.meaning = meaning
        self.frequency = frequency
        self.level = level
    }
    
    init(snapshot :FIRDataSnapshot){
        let snapshotValue = snapshot.value as! [String:Any]
        self.pinyin = snapshotValue["pinyin"] as? String
        self.cnChar = snapshotValue["cnCount"] as? String
        self.meaning = snapshotValue["meaning"] as? String
        self.frequency = snapshotValue["frequency"] as? Int
        self.sentences = snapshotValue["sentences"] as? [String:AnyObject]
        self.level = snapshotValue["level"] as? Int
        
    }
    
}
