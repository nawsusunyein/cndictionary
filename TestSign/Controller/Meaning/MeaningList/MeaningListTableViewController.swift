//
//  MeaningListTableViewController.swift
//  TestSign
//
//  Created by techfun on 2018/11/28.
//  Copyright © 2018 Naw Su Su Nyein. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Firebase

class MeaningListTableViewController: UITableViewController {
    
    
    @IBOutlet var meaningTable: UITableView!
    
    private var dbRef = FIRDatabaseReference()
    private var dbHandle : FIRDatabaseHandle!
    
    private var vocabList = [MeaningModel]()
    private var selectedVocab : MeaningModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        meaningTable.register(UINib(nibName: "MeaningListTableViewCell", bundle: nil), forCellReuseIdentifier: "meaningListTableViewCell")
        
        FIRApp.configure()
        self.dbRef = FIRDatabase.database().reference().child("vocabulary")
        self.getVocabList()
    }

    //Firebase Retrieve Method
    func getVocabList(){
       dbRef.observeSingleEvent(of: .value, with: {(snapshot) in
            print("snap shot children count = \(snapshot.childrenCount)")
            for eachVocab in snapshot.children{
                let vocab = MeaningModel(snapshot:eachVocab as! FIRDataSnapshot)
                self.vocabList.append(vocab)
            }
        
            self.meaningTable.reloadData()
        })
      
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.vocabList.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "meaningListTableViewCell", for: indexPath) as? MeaningListTableViewCell
        let vocab = self.vocabList[indexPath.row]
        cell?.cnChar.text = vocab.cnChar
        cell?.pinyin.text = vocab.pinyin
        // Configure the cell...

        return cell!
    }
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        print("selected table : \(self.vocabList[indexPath.row].cnChar)")
        self.selectedVocab = self.vocabList[indexPath.row]
        self.performSegue(withIdentifier: "showMeaningDetail", sender: nil)
    }
  
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        switch segue.identifier{
        case "showMeaningDetail":
            if let meaningDetailVC = segue.destination as? MeaningDetailViewController
            {
                meaningDetailVC.vocabDetail = self.selectedVocab
            }
        default:
            break
        }
    }
    

}
