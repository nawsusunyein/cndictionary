//
//  MeaningDetailViewController.swift
//  TestSign
//
//  Created by techfun on 2018/11/29.
//  Copyright © 2018 Naw Su Su Nyein. All rights reserved.
//

import UIKit
import CoreText
import Foundation
import QuartzCore
import PocketSVG
import SwiftSVG
import SVGPath
import WebKit

class MeaningDetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,WKNavigationDelegate{
   
    

    @IBOutlet weak var txtCnChar : UILabel?
    @IBOutlet weak var txtPinYin : UILabel?
    @IBOutlet weak var copyBtn : UIButton?
    @IBOutlet weak var favouriteBtn : UIButton?
    @IBOutlet weak var pronounceBtn : UIButton?
    @IBOutlet weak var dictionaryBtn : UIButton?
    @IBOutlet weak var sentenceBtn : UIButton?
    @IBOutlet weak var strokeBtn : UIButton?
    @IBOutlet weak var dictionaryBar : UIView?
    @IBOutlet weak var sentenceBar : UIView?
    @IBOutlet weak var strokeBar : UIView?
    @IBOutlet weak var strokeView : UIView?
    @IBOutlet weak var dictionaryView : UIView?
    @IBOutlet weak var vocabMeaningText : UILabel?
    @IBOutlet weak var sentenceTableView : UITableView?
    @IBOutlet weak var webView: WKWebView!
    private var curSvgFileName : String?
    
    public var vocabDetail : MeaningModel!
    private var sentenceArrayList = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sentenceTableView?.register(UINib(nibName: "MeaningDetailSentenceTableViewCell", bundle: nil), forCellReuseIdentifier: "meaningDetailSentenceCell")
        self.setInitialViewLayout()
        self.setVocabData()
        //self.prepareSentences()
        
    }
    
    //Private Function
    func setInitialViewLayout(){
        self.sentenceTableView?.isHidden = true
        self.strokeView?.isHidden = true
        self.dictionaryView?.isHidden = false
        self.strokeBar?.backgroundColor = UIColor.clear
        self.sentenceBar?.backgroundColor = UIColor.clear
    }
    
    func setVocabData(){
        self.txtCnChar?.text = self.vocabDetail.cnChar
        self.txtPinYin?.text = self.vocabDetail.pinyin
        self.vocabMeaningText?.text = self.vocabDetail.meaning
    }
    
    func prepareSentences(){
        if self.sentenceArrayList.count == 0{
            for value in (vocabDetail.sentences?.values)!{
                print("sentence list : \(value.description!)")
                self.sentenceArrayList.append(value.description!)
            }
        }
        
        self.sentenceTableView?.isHidden = false
        self.sentenceTableView?.reloadData()
    }
    
    func showSVGImage(_ isFromReload:Bool){
        var str : String = self.vocabDetail.cnChar ?? ""
        //var str : String = "上月"
        
        var svgFileArr = [String]()
       
        for val in str.unicodeScalars{
            svgFileArr.append(String(val.value))
        }
        let svgFileCount = svgFileArr.count
        
     
            if (self.curSvgFileName == nil){
                self.curSvgFileName = svgFileArr[0]
                
            }else{
                if svgFileCount == 1{
                    self.curSvgFileName = svgFileArr[0]
                }else{
                    if let fileIndex = svgFileArr.lastIndex(where: {$0 == self.curSvgFileName}){
                        if(isFromReload){
                            self.curSvgFileName = svgFileArr[fileIndex]
                        }else{
                            if(fileIndex < svgFileCount - 1){
                                self.curSvgFileName = svgFileArr[fileIndex + 1]
                            }else{
                                self.curSvgFileName = svgFileArr[fileIndex - 1]
                            }
                        }
                        
                    }
                }
               
            }
        
        
        let path: String = Bundle.main.path(forResource: curSvgFileName, ofType: "svg")!
        let url: NSURL = NSURL.fileURL(withPath: path) as NSURL
        let request: NSURLRequest = NSURLRequest(url: url as URL)
        webView.load(request as URLRequest)
        
    }
    
   
    @IBAction func showPreviousCharacters(_ sender: Any) {
        self.showSVGImage(false)
    }
    
    
    @IBAction func showNextCharacters(_ sender: Any) {
        self.showSVGImage(false)
    }
    
    
    @IBAction func replayCharacters(_ sender: Any) {
        self.showSVGImage(true)
    }
    
   
    
    //IBAction
    
    @IBAction func showVocabMeaning(_ sender: Any) {
       self.dictionaryBtn?.backgroundColor = UIColor(red: 255/255, green: 195/255, blue: 21/255, alpha: 1)
        self.dictionaryBar?.backgroundColor = UIColor(red: 15/255, green: 72/255, blue: 20/255, alpha: 1)
        self.strokeBtn?.backgroundColor = UIColor.clear
        self.strokeBar?.backgroundColor = UIColor.clear
        self.sentenceBtn?.backgroundColor = UIColor.clear
        self.sentenceBar?.backgroundColor = UIColor.clear
        self.sentenceTableView?.isHidden = true
        self.strokeView?.isHidden = true
        self.dictionaryView?.isHidden = false
    }
    
    @IBAction func showVocabSentences(_ sender: Any) {
        self.sentenceBtn?.backgroundColor = UIColor(red: 255/255, green: 195/255, blue: 21/255, alpha: 1)
        self.sentenceBar?.backgroundColor = UIColor(red: 15/255, green: 72/255, blue: 20/255, alpha: 1)
        self.dictionaryBtn?.backgroundColor = UIColor.clear
        self.dictionaryBar?.backgroundColor = UIColor.clear
        self.strokeBtn?.backgroundColor = UIColor.clear
        self.strokeBar?.backgroundColor = UIColor.clear
        self.prepareSentences()
        self.strokeView?.isHidden = true
        self.dictionaryView?.isHidden = true
    }
    
    @IBAction func showStroke(_ sender: Any) {
        self.strokeBtn?.backgroundColor = UIColor(red: 255/255, green: 195/255, blue: 21/255, alpha: 1)
        self.strokeBar?.backgroundColor = UIColor(red:15/255, green: 72/255, blue: 20/255, alpha: 1)
        self.dictionaryBtn?.backgroundColor = UIColor.clear
        self.dictionaryBar?.backgroundColor = UIColor.clear
        self.sentenceBtn?.backgroundColor = UIColor.clear
        self.sentenceBar?.backgroundColor = UIColor.clear
        self.strokeView?.isHidden = false
        self.dictionaryView?.isHidden = true
        self.sentenceTableView?.isHidden = true
        
        self.showSVGImage(false)
        //self.createStroke()
    }
    
    @IBAction func copyVocab(_ sender: Any) {
        UIPasteboard.general.string = self.vocabDetail.cnChar
    }
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if((self.sentenceArrayList.count) > 0)
            {
                return (self.sentenceArrayList.count)
        }
        return 0
    }
    
    
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "meaningDetailSentenceCell", for: indexPath) as! MeaningDetailSentenceTableViewCell
        cell.cnSentence.text = self.sentenceArrayList[indexPath.row]
        // Configure the cell...
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
       
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String{
    func utf8DecodedString()-> String {
        let data = self.data(using: .utf8)
        if let message = String(data: data!, encoding: .nonLossyASCII){
            return message
        }
        return ""
    }
    
    func utf8EncodedString()-> String {
        let messageData = self.data(using: .nonLossyASCII)
        let text = String(data: messageData!, encoding: .utf8)
        return text ?? ""
    
}
}


